<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group(['middleware' => 'auth:api'], function () {
    Route::post('logout', 'Auth\LoginController@logout');

    Route::get('/user', function (Request $request) {
        return $request->user();
    });

    Route::post('addclient', 'ClientsController@create');
    Route::post('addproject', 'ProjectsController@create');
    Route::post('addtask', 'TasksController@create');
    Route::post('completeproject', 'CompleteProjectsController@create');

    Route::get('/clients', 'ClientsController@show'); // Get Clients
    Route::get('/projects/{id}', 'ProjectsController@show'); // Get Projects
    Route::get('/completeprojects', 'CompleteProjectsController@show');
    Route::get('/tasks/{id}', 'TasksController@show'); // Get Tasks

    Route::patch('/clients/{id}', 'ClientsController@update'); // Update Client
    Route::patch('/projects/{id}', 'ProjectsController@update'); // Update Project
    Route::patch('/tasks/{id}', 'TasksController@update'); // Update Task

    Route::delete('/clients/{id}', 'ClientsController@destroy'); // Delete Client
    Route::delete('/projects/{id}', 'ProjectsController@destroy'); // Delete Project
    Route::delete('/tasks/{id}', 'TasksController@destroy'); // Delete Task

    Route::patch('settings/profile', 'Settings\ProfileController@update');
    Route::patch('settings/password', 'Settings\PasswordController@update');

    Route::post('/upload', 'Settings\ProfileController@uploadImage');
});

Route::group(['middleware' => 'guest:api'], function () {
    Route::post('login', 'Auth\LoginController@login');
    Route::post('register', 'Auth\RegisterController@register');
    Route::post('password/email', 'Auth\ForgotPasswordController@sendResetLinkEmail');
    Route::post('password/reset', 'Auth\ResetPasswordController@reset');

    Route::post('oauth/{driver}', 'Auth\OAuthController@redirectToProvider');
    Route::get('oauth/{driver}/callback', 'Auth\OAuthController@handleProviderCallback')->name('oauth.callback');
});
