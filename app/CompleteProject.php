<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CompleteProject extends Model
{
    //Table name
    protected $table = 'complete_projects';

    protected $fillable = [
        'user_id', 'client_id', 'original_id', 'projectName', 'isNotTimed', 'hourlyOverride', 'totalHoursWorked'
    ];

    //Primary Key
    public $primaryKey = 'id';

    //Timestamps
    public $timestamps = true;

    public function clients(){
        return $this->belongsTo('App\Client');
    }

    public function tasks(){
        return $this->hasMany('App\Task');
    }

    public function user(){
        return $this->belongsTo('App\User');
    }
}