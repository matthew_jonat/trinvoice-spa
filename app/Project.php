<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Project extends Model
{
    //Table name
    protected $table = 'projects';

    protected $fillable = [
        'client_id', 'original_id', 'projectName', 'isNotTimed', 'hourlyOverride', 'totalHoursWorked', 'isRestored',
    ];

    //Primary Key
    public $primaryKey = 'id';

    //Timestamps
    public $timestamps = true;

    public function clients(){
        return $this->belongsTo('App\Client');
    }

    public function tasks(){
        return $this->hasMany('App\Task');
    }
}