<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Client extends Model
{
    //Table name
    protected $table = 'clients';

    protected $fillable = [
        'user_id', 'clientName', 'clientAddress'
    ];

    //Primary Key
    public $primaryKey = 'id';

    //Timestamps
    public $timestamps = true;

    public function user(){
        return $this->belongsTo('App\User');
    }

    public function projects(){
        return $this->hasMany('App\Project');
    }
}
