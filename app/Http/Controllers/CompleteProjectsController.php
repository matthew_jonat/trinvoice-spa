<?php

namespace App\Http\Controllers;

use App\User;
use App\CompleteProject;

use Illuminate\Http\Request;

class CompleteProjectsController extends Controller
{
    public function create(Request $request)
    {
        // Create Project for this client
        return CompleteProject::create([
            'user_id' => $request->user()->id,
            'client_id' => $request['client_id'],
            'original_id' => $request['id'],
            'projectName' => $request['projectName'],
            'isNotTimed' => $request['isNotTimed'],
            'hourlyOverride' => $request['hourlyOverride'],
            'totalHoursWorked' => $request['totalHoursWorked']
        ]);
    }

    public function show(Request $request){
        $userId = $request->user()->id;
        return User::find($userId)->completeProjects;
    }
}
