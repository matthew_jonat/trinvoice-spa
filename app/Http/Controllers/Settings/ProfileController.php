<?php

namespace App\Http\Controllers\Settings;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Carbon\Carbon;
use Validator;
use Image;
use File;

class ProfileController extends Controller
{
    /**
     * Update the user's profile information.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {

        $user = $request->user();

        $this->validate($request, [
            'firstName' => 'required',
            'lastName' => 'required',
            'hourlyRate' => 'numeric',
            'email' => 'required|email|unique:users,email,'.$user->id,
        ]);

        return tap($user)->update($request->only('firstName', 'lastName', 'email', 'phone', 'companyName', 'companyLogo', 'companyURL', 'companyAddress', 'hourlyRate'));
    }

    public function uploadImage(Request $request)
    {
        $userId = $request->user()->id;
        $imagePath = public_path('storage/'.$userId.'/');
        if (!File::exists($imagePath)) {
            File::makeDirectory($imagePath);
        }
        $fileName = "";

        $validator = Validator::make($request->all(), [
            'image' => 'image64:jpeg,jpg,png'
        ]);
        if ($validator->fails()) {
            return response()->json(['errors'=>$validator->errors()]);
        } else {
            $imageData = $request->get('image');
            $fileName = Carbon::now()->timestamp . '_' . uniqid() . '.' . explode('/', explode(':', substr($imageData, 0, strpos($imageData, ';')))[1])[1];
            Image::make($request->get('image'))->save($imagePath.$fileName);

            $imagePath = '/storage/'.$userId.'/'.$fileName;

            $user = \App\User::find($userId);
            $user->companyLogo = $imagePath;
            $user->save();

            return response()->json(['error'=>false]);
        }
    }
}
