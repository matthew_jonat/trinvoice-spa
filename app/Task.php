<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Task extends Model
{
    // Table name
    protected $table = 'tasks';

    protected $fillable = [
        'project_id', 'taskName', 'start', 'stop', 'elapsed', 'offset', 'timeStr', 'hasStarted', 'isComplete'
    ];

    // Append inProgress field that doesnt really need to go in the DB.
    protected $appends = [
        'inProgress',
        'si'
    ];

    /**
     * Get the in progress attribute....I have no idea how this works but it does.
     *
     * @return string
     */
    public function getInProgressAttribute()
    {
        return false;
    }

    public function getSiAttribute()
    {
        return -1;
    }

    //Primary Key
    public $primaryKey = 'id';

    //Timestamps
    public $timestamps = true;

    public function projects(){
        return $this->belongsTo('App\Project');
    }

    public function completeProjects(){
        return $this->belongsTo('App\CompleteProject');
    }
}
