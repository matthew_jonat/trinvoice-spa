import Vue from 'vue'
import { library } from '@fortawesome/fontawesome-svg-core'
import { FontAwesomeIcon } from '@fortawesome/vue-fontawesome'

// import { } from '@fortawesome/fontawesome-free-regular/shakable.es'

import {
    faUser, faLock, faSignOutAlt, faCog, faChevronRight, faPlusSquare, faPlusCircle, faArrowAltCircleLeft, faPlay, faPause, faTrashAlt, faCheck, faInfo, faFileInvoice
} from '@fortawesome/free-solid-svg-icons';

import {
    faGithub
} from '@fortawesome/free-brands-svg-icons';

library.add(
    faUser, faLock, faSignOutAlt, faCog, faGithub, faChevronRight, faPlusSquare, faPlusCircle, faArrowAltCircleLeft, faPlay, faPause, faTrashAlt, faCheck, faInfo, faFileInvoice
);

Vue.component('fa', FontAwesomeIcon);
