import axios from 'axios';
import Cookies from 'js-cookie';
import * as types from '../mutation-types';

// state
export const state = {
    user: null,
    tabs: [
        { id: 1, name: 'clients', open: true },
        { id: 2, name: 'projects', open: false },
        { id: 3, name: 'tasks', open: false }
    ],
    innerPages: [
        { id: 1, name: 'add-client', show: true },
        { id: 2, name: 'add-project', show: false },
        { id: 3, name: 'add-task', show: false },
        { id: 4, name: 'task-timer', show: false }
    ],
    modals: [
        { id: 1, name: 'client', open: false },
        { id: 2, name: 'project', open: false },
        { id: 3, name: 'task', open: false },
        { id: 4, name: 'project-settings', open: false },
        { id: 5, name: 'client-settings', open: false },
        { id: 6, name: 'delete-client', open: false },
        { id: 7, name: 'delete-project', open: false },
        { id: 8, name: 'task-settings', open: false },
        { id: 9, name: 'delete-task', open: false },
        { id: 10, name: 'project-info', open: false },
        { id: 11, name: 'create-quote', open: false },
        { id: 12, name: 'create-invoice', open: false },
        { id: 13, name: 'complete-project', open: false },
        { id: 14, name: 'warn-complete-user-profile', open: false },
        { id: 15, name: 'warn-complete-company-info', open: false }
    ],
    clients: null,
    client: null,
    projects: null,
    project: null,
    completeProjects: null,
    tasks: null,
    task: null,
    loading: false,
    token: Cookies.get('token')
}

// getters
export const getters = {
    user: state => state.user,
    tabs: state => state.tabs,
    innerPages: state => state.innerPages,
    modals: state => state.modals,
    clients: state => state.clients,
    client: state => state.client,
    projects: state => state.projects,
    project: state => state.project,
    completeProjects: state => state.completeProjects,
    tasks: state => state.tasks,
    task: state => state.task,
    token: state => state.token,
    loading: state => state.loading,
    check: state => state.user !== null
}

// mutations
export const mutations = {
    [types.SAVE_TOKEN] (state, { token, remember }) {
        state.token = token;
        Cookies.set('token', token, { expires: remember ? 365 : null });
    },

    [types.FETCH_USER_SUCCESS] (state, { user }) {
        state.user = user;
    },

    [types.FETCH_USER_FAILURE] (state) {
        state.token = null;
        Cookies.remove('token');
    },

    [types.LOGOUT] (state) {
        state.user = null;
        state.token = null;

        Cookies.remove('token');
    },

    [types.UPDATE_USER] (state, { user }) {
        state.user = user;
    },

    [types.FETCH_CLIENT] (state, { clients }) {
        state.clients = clients;
    },

    [types.SET_CLIENT] (state, clientId) {
        const client = state.clients.find(client => {
            return client.id === clientId;
        });
        state.client = client;
    },

    [types.UPDATE_CLIENT] (state, { client }) {
        const clients = state.clients.find(clients => {
            return clients.id === client.id;
        });

        clients.clientName = client.clientName;
        clients.clientAddress = client.clientAddress;

        // update singular client
        state.client = client;
    },

    [types.DELETE_CLIENT] (state, clientId) {
        state.clients.forEach(function (val, index) {
            if (val.id === clientId) {
                state.clients.splice(index, 1);
            }
        })
    },

    [types.FETCH_PROJECTS] (state, { projects }) {
        state.projects = projects;
    },

    [types.SET_PROJECT] (state, projectId) {
        const project = state.projects.find(project => {
            return project.id === projectId;
        });
        state.project = project;
    },

    [types.UPDATE_PROJECTS] (state, { project }) {
        const projects = state.projects.find(projects => {
            return projects.id === project.id;
        });

        projects.projectName = project.projectName;
        projects.isNotTimed = project.isNotTimed;
        projects.hourlyOverride = project.hourlyOverride;

        // update singular project
        state.project = project;
    },

    [types.DELETE_PROJECT] (state, projectId) {
        state.projects.forEach(function (val, index) {
            if (val.id === projectId) {
                state.projects.splice(index, 1);
            }
        })
    },

    [types.UPDATE_PROJECT_HOURS] (state, payload) {
        // Update the project total hours worked
        let totalHoursWorked = 0;

        state.tasks.forEach(task => {
            totalHoursWorked += task.elapsed;
        });

        state.project.totalHoursWorked = totalHoursWorked;

        const currentProject = state.projects.find(id => {
            return id.id === payload.projectId;
        });

        currentProject.totalHoursWorked = totalHoursWorked;

        axios.patch('api/projects/' + payload.projectId, {
            'totalHoursWorked': totalHoursWorked
        });
    },

    [types.FETCH_COMPLETE_PROJECTS] (state, { completeProjects }) {
        state.completeProjects = completeProjects;
    },

    [types.FETCH_TASKS] (state, { tasks }) {
        state.tasks = tasks;
    },

    [types.SET_TASK] (state, taskId) {
        const task = state.tasks.find(task => {
            return task.id === taskId;
        });
        state.task = task;
    },

    [types.UPDATE_TASK] (state, task) {
        const tasks = state.tasks.find(tasks => {
            return tasks.id === task.id;
        });

        tasks.taskName = task.taskName;
        tasks.start = task.start;
        tasks.stop = task.stop;
        tasks.elapsed = task.elapsed;
        tasks.offset = task.offset;
        tasks.timeStr = task.timeStr;
        tasks.hasStarted = task.hasStarted;
        tasks.isComplete = task.isComplete;

        // update singular project
        state.task = task;
    },

    [types.DELETE_TASK] (state, taskId) {
        state.tasks.forEach(function (val, index) {
            if (val.id === taskId) {
                state.tasks.splice(index, 1);
            }
        })
    },

    [types.COMPLETE_TASK] (state, task) {
        const tasks = state.tasks.find(tasks => {
            return tasks.id === task.id;
        });

        tasks.isComplete = true;
        state.task.isComplete = true;

        // Save to the database
        axios.patch('/api/tasks/' + task.id, {
            'isComplete': true
        });
    },

    toggleTab (state, tabId) {
        for (var i = 0; i <= state.tabs.length - 1; i++) {
            state.tabs[i].open = false;
        }

        const tab = state.tabs.find(tab => {
            return tab.id === tabId;
        });
        tab.open = !tab.open;
    },

    [types.MODAL] (state, modalName) {
        const modal = state.modals.find(modal => {
            return modal.name === modalName;
        });
        modal.open = !modal.open;
    },

    [types.LOADING] (state, bool) {
        state.loading = bool;
    },

    [types.INNER_PAGE] (state, pageName) {
        state.innerPages.forEach(function (val) {
            val.show = false;
        });

        const page = state.innerPages.find(page => {
            return page.name === pageName;
        });

        page.show = !page.show;
    },

    [types.START_TIMER] (state, timerId) {
        const currentTimer = state.tasks.find(id => {
            return id.id === timerId;
        });

        if (!currentTimer.hasStarted) {
            currentTimer.start = new Date().getTime();
        }

        currentTimer.hasStarted = true;
        currentTimer.inProgress = true;

        // if there is a stop time, then we calculate the offset.
        if (currentTimer.stop !== null) {
            currentTimer.offset = Math.floor(((new Date()).getTime() - currentTimer.stop) / 1000) + currentTimer.offset;
            currentTimer.stop = null;
        }

        // Create the timer.
        currentTimer.si = setInterval (function () {

            let tick = new Date();
            let elapsedTimeInSeconds = (tick.getTime() - currentTimer.start) / 1000;
            currentTimer.elapsed = Math.floor(elapsedTimeInSeconds - currentTimer.offset);

            let secs = currentTimer.elapsed;
            let hrs = Math.floor(secs / 3600);
            secs %= 3600;
            let mns = Math.floor(secs / 60);
            secs %= 60;
            currentTimer.timeStr = (hrs < 10 ? '0' : '') + hrs + ':' + (mns < 10 ? '0' : '') + mns + ':' + (secs < 10 ? '0' : '') + secs;

            state.task = currentTimer;

        }, 100);
    },

    [types.STOP_TIMER] (state, payload) {
        const currentTimer = state.tasks.find(id => {
            return id.id === payload.taskId;
        });

        // Remember the paused time, so that we can calculate the offset when we resume.
        currentTimer.stop = new Date().getTime();

        // stopping the timer.
        clearInterval(currentTimer.si);
        currentTimer.si = -1;
        currentTimer.inProgress = false;

        // Save to the database
        axios.patch('/api/tasks/' + currentTimer.id, {
            'start': currentTimer.start,
            'stop': currentTimer.stop,
            'elapsed': currentTimer.elapsed,
            'offset': currentTimer.offset,
            'timeStr': currentTimer.timeStr,
            'hasStarted': currentTimer.hasStarted
        });

        state.task = currentTimer;
    },

    [types.STOP_ALL_TIMERS] (state) {
        state.tasks.forEach(task => {
            if (task.inProgress) {
                task.stop = new Date().getTime();
                clearInterval(task.si);
                task.si = -1;
                task.inProgress = false;

                axios.patch('/api/tasks/' + task.id, {
                    'start': task.start,
                    'stop': task.stop,
                    'elapsed': task.elapsed,
                    'offset': task.offset,
                    'timeStr': task.timeStr,
                    'hasStarted': task.hasStarted
                });
            }
        });
    }

}

// actions
export const actions = {
    saveToken ({ commit, dispatch }, payload) {
        commit(types.SAVE_TOKEN, payload)
    },

    async fetchUser ({ commit }) {
        try {
            const { data } = await axios.get('/api/user');

            commit(types.FETCH_USER_SUCCESS, { user: data });
        } catch (e) {
            commit(types.FETCH_USER_FAILURE);
        }
    },

    updateUser ({ commit }, payload) {
        commit(types.UPDATE_USER, payload)
    },

    async fetchClient ({ commit }) {
        const response = await axios.get('/api/clients');

        commit(types.FETCH_CLIENT, { clients: response.data });
    },

    setClient ({ commit }, clientId) {
        commit(types.SET_CLIENT, clientId);
    },

    updateClient ({ commit }, payload) {
        commit(types.UPDATE_CLIENT, payload);
    },

    deleteClient ({ commit }, clientId) {
        commit(types.DELETE_CLIENT, clientId);
    },

    async fetchProjects ({ commit }, clientId) {
        const response = await axios.get('/api/projects/' + clientId);

        commit(types.FETCH_PROJECTS, {projects: response.data});
    },

    setProject ({ commit }, projectId) {
        commit(types.SET_PROJECT, projectId);
    },

    updateProject ({ commit }, payload) {
        commit(types.UPDATE_PROJECTS, payload);
    },

    async logout ({ commit }) {
        try {
            await axios.post('/api/logout');
        } catch (e) { }

        commit(types.LOGOUT);
    },

    async fetchOauthUrl (ctx, { provider }) {
        const { data } = await axios.post(`/api/oauth/${provider}`);

        return data.url;
    },

    loadClientProjects ({ commit }, payload) {
        commit('toggleTab', payload.tabId);
        commit(types.SET_CLIENT, payload.clientId);
    },

    loadProjectTasks ({ commit }, payload) {
        commit('toggleTab', payload.tabId);
        commit(types.SET_PROJECT, payload.projectId);
    },

    deleteProject ({ commit }, projectId) {
        commit(types.DELETE_PROJECT, projectId);
    },

    setTask ({ commit }, payload) {
        commit(types.STOP_ALL_TIMERS);
        commit(types.SET_TASK, payload.taskId);
        commit(types.UPDATE_PROJECT_HOURS, payload);
    },

    updateTask ({ commit }, payload) {
        commit(types.UPDATE_TASK, payload);
    },

    deleteTask ({ commit }, taskId) {
        commit(types.DELETE_TASK, taskId);
    },

    async completeTask ({ commit }, task) {
        if (!task.hasStarted) {
            await commit(types.STOP_TIMER, task.id);
        }
        commit(types.COMPLETE_TASK, task);
    },

    toggleModal ({ commit }, modalName) {
        commit(types.MODAL, modalName);
    },

    loading ({ commit }, bool) {
        commit(types.LOADING, bool);
    },

    async fetchTasks ({ commit }, projectId) {
        const response = await axios.get('/api/tasks/' + projectId);

        commit(types.FETCH_TASKS, {tasks: response.data});
    },

    togglePage ({ commit }, pageName) {
        commit(types.INNER_PAGE, pageName);
    },

    async startTimer ({ commit }, task) {
        await commit(types.START_TIMER, task.id);
    },

    pauseTimer ({ commit }, payload) {
        commit(types.STOP_TIMER, payload);
        commit(types.UPDATE_PROJECT_HOURS, payload)
    },

    async fetchCompleteProjects ({ commit }) {
        const response = await axios.get('/api/completeprojects');

        commit(types.FETCH_COMPLETE_PROJECTS, { completeProjects: response.data });
    }
}
