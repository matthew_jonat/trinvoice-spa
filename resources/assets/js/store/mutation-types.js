// auth.js
export const LOGOUT = 'LOGOUT';
export const SAVE_TOKEN = 'SAVE_TOKEN';
export const FETCH_USER = 'FETCH_USER';
export const FETCH_USER_SUCCESS = 'FETCH_USER_SUCCESS';
export const FETCH_USER_FAILURE = 'FETCH_USER_FAILURE';
export const UPDATE_USER = 'UPDATE_USER';

// Clients
export const FETCH_CLIENT = 'FETCH_CLIENT';
export const SET_CLIENT = 'SET_CLIENT';
export const UPDATE_CLIENT = 'UPDATE_CLIENT';
export const DELETE_CLIENT = 'DELETE_CLIENT';

// Projects
export const FETCH_PROJECTS = 'FETCH_PROJECT';
export const SET_PROJECT = 'SET_PROJECT';
export const UPDATE_PROJECTS = 'UPDATE_PROJECTS';
export const UPDATE_PROJECT_HOURS = 'UPDATE_PROJECT_HOURS';
export const DELETE_PROJECT = 'DELETE_PROJECT';

export const FETCH_COMPLETE_PROJECTS = 'FETCH_COMPLETE_PROJECTS';

// Tasks
export const FETCH_TASKS = 'FETCH_TASKS';
export const SET_TASK = 'SET_TASK';
export const UPDATE_TASK = 'UPDATE_TASK';
export const DELETE_TASK = 'DELETE_TASK';
export const COMPLETE_TASK = 'COMPLETE_TASK';

// Tabs
export const TAB = 'TAB';

// Inner Pages
export const INNER_PAGE = 'INNER_PAGE';

// Modal
export const MODAL = 'MODAL';

// lang.js
export const SET_LOCALE = 'SET_LOCALE';

// Other
export const LOADING = 'LOADING';

// Timers
export const ADD_TIMER_CHECK = 'ADD_TIMER_CHECK';
export const START_TIMER = 'START_TIMER';
export const STOP_TIMER = 'STOP_TIMER';
export const STOP_ALL_TIMERS = 'STOP_ALL_TIMERS';
