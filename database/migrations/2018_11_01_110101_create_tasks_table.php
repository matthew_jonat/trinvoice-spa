<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTasksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tasks', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('project_id');
            $table->string('taskName')->nullable();
            $table->bigInteger('start')->nullable();
            $table->bigInteger('stop')->nullable();
            $table->integer('elapsed')->default(0);
            $table->integer('offset')->default(0);
            $table->string('timeStr')->default('00:00:00');
            $table->boolean('hasStarted')->default(false);
            $table->boolean('isComplete')->default(false);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tasks');
    }
}
